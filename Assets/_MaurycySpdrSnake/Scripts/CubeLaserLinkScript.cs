﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//class controls lasers of laserObjects
//needs:
//laser source prefab
//laser object mesh model tag
//number of tile layer
public class CubeLaserLinkScript : MonoBehaviour {
    public List<GameObject> _neighbors = new List<GameObject>();
    [SerializeField] GameObject _laserSource;
    private List<GameObject> _laserSources = new List<GameObject>();
    private List<LineRenderer> _laserLines = new List<LineRenderer>();
    public int ID { get; set; }
    private bool objectStarted = false;
    private LaserActivationScript tileLaserScript = null;
    [SerializeField] string laserObjectMeshTag = "LaserCube";
    [SerializeField] string activatedTag = "LaserObjectActive";
    [SerializeField] int tileLayer = 8;

    //private void Start()
    //{
    //    if (!objectStarted)
    //    {
    //        objectStarted = true;
    //    }
    //}

    private void OnTriggerEnter(Collider other) //makes connections between neigboring laser objects and with its tile on entering
    {
        //if (!objectStarted) 
        //{
        //    Start();
        //}
        
        if (other.CompareTag(laserObjectMeshTag) && this.gameObject != other.gameObject.transform.parent.gameObject)
        {
            _neighbors.Add(other.gameObject.transform.parent.gameObject);
            GameObject temp;
            if (other.gameObject.transform.parent.gameObject.GetComponent<CubeLaserLinkScript>()._neighbors.Contains(this.gameObject))
            {
                _laserSources.Add(null);
                _laserLines.Add(null);
            }
            else
            {
                temp = (GameObject)Instantiate(_laserSource);
                temp.transform.position = this.transform.position;
                temp.transform.parent = this.gameObject.transform;
                _laserSources.Add(temp);
                _laserLines.Add(temp.GetComponent<LineRenderer>());
                LineRenderer tempLineRednerer = _laserLines[_laserSources.Count - 1];
                tempLineRednerer.material.color = Color.red;
                tempLineRednerer.enabled = true;
                tempLineRednerer.SetPosition(0, this.transform.position);
                tempLineRednerer.SetPosition(1, other.gameObject.transform.position);
            }
        }
        if (other.gameObject.layer == tileLayer)
        {
            tileLaserScript = other.gameObject.GetComponent<LaserActivationScript>();
            print("Laser knows his tile.");
        }
    }

    private void OnDestroy()//delets connections between neigboring laser objects and with its tile on entering
    {
        foreach (GameObject neighbor in _neighbors)
        {
            neighbor.GetComponent<CubeLaserLinkScript>().DeactivateLasers(this.gameObject);
        }
        if (tileLaserScript != null) {
            List<CubeLaserLinkScript> laserObjects = tileLaserScript.laserObjects;
            int i = laserObjects.FindIndex(laser => laser == this);
            laserObjects.RemoveAt(i);
        }
    }

    public void DeactivateLasers(GameObject otherDeletedObject)//removes neigbor and lasers leading to him from its structure
    {
        int i = _neighbors.FindIndex(neighbour => neighbour == otherDeletedObject);
        GameObject laserSourceToDelete = _laserSources[i];
        _neighbors.RemoveAt(i);
        _laserSources.RemoveAt(i);
        _laserLines.RemoveAt(i);
        Destroy(laserSourceToDelete);
    }

    public void ActivateLaser(Color color)//changes color of laser
    {
        foreach (LineRenderer laser in _laserLines)
        {
            if (laser != null) laser.material.color = color;
        }
        this.gameObject.tag = activatedTag;
        print(activatedTag);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ActivateLaser(Color.green);
        }
    }
}
