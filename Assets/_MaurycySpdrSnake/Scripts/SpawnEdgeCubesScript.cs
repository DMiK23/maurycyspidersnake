﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Class spawns _cubes in a rectangle 2*_width wide and 2*_height high, they are _space apart and on _cubeHeight height
//needs all parameters and laser object prefab
public class SpawnEdgeCubesScript : MonoBehaviour {
    [SerializeField] GameObject _cube;
    [SerializeField] float _width = 34f;
    [SerializeField] float _height = 18f;
    [SerializeField] float _space = 2f;
    [SerializeField] float _cubeHeight = 0.25f;
    void Start()
    {
        for (int j = -1; j < 2; j += 2) //spawns bottom and top _cubes
        {
            for (float i = -_width; i <= _width; i += _space)
            {
                GameObject temp;
                temp = (GameObject)Instantiate(_cube);
                temp.transform.position = new Vector3(j * _height, _cubeHeight, i);
            }
        }
        for (int j = -1; j < 2; j += 2)//spawns left and right _cubes
        {
            for (float i = -(_height - _space); i <= (_height - _space); i += _space)
            {
                GameObject temp;
                temp = (GameObject)Instantiate(_cube);
                temp.transform.position = new Vector3(i, _cubeHeight, j * _width);
            }
        }

    }



}
