﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//class for a tile, allows it to remember Laser objent standing on them and activate laser on it
//needs Tag of laser object
public class LaserActivationScript : MonoBehaviour {
    public List<CubeLaserLinkScript> laserObjects = new List<CubeLaserLinkScript>();
    [SerializeField] string _laseObjectTag = "LaserObject";


    public void ActivateLaserOnThisTile() //changer color of laser to green
    {
        if (laserObjects.Count > 0)
            foreach (CubeLaserLinkScript laserItem in laserObjects)
                laserItem.ActivateLaser(Color.green);            
    }

    private void OnTriggerEnter(Collider other) //remembers laser objects placed upon it
    {
        if (other.CompareTag(_laseObjectTag))
        {
            laserObjects.Add(other.gameObject.GetComponent<CubeLaserLinkScript>());
        }
    }
}
